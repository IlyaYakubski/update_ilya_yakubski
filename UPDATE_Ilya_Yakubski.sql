UPDATE film
SET rental_duration = 21, rental_rate = 9.99
WHERE title = 'THE TERMINATOR';

UPDATE customer
SET
    first_name = 'ILYA',
    last_name = 'YAKUBSKI',
    email = 'yakubski.ilya@student.ehu.lt',
    address_id = (SELECT address_id FROM address LIMIT 1)
WHERE customer_id IN (
    SELECT customer_id
    FROM (
        SELECT rental.customer_id
        FROM rental
        JOIN payment ON rental.customer_id = payment.customer_id
        GROUP BY rental.customer_id
        HAVING COUNT(rental.rental_id) >= 10 AND COUNT(payment.payment_id) >= 10
        LIMIT 1
    ) AS subquery
);

UPDATE customer
SET
    create_date = CURRENT_DATE
WHERE customer_id IN (
    SELECT customer_id
    FROM (
        SELECT rental.customer_id
        FROM rental
        JOIN payment ON rental.customer_id = payment.customer_id
        GROUP BY rental.customer_id
        HAVING COUNT(rental.rental_id) >= 10 AND COUNT(payment.payment_id) >= 10
        LIMIT 1
    ) AS subquery
);